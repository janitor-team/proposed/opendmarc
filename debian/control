Source: opendmarc
Section: mail
Priority: optional
Maintainer: Scott Kitterman <scott@kitterman.com>
Uploaders: David Bürgin <dbuergin@gluet.ch>
Build-Depends: debhelper-compat (= 13), libmilter-dev, pkg-config, miltertest,
 libspf2-dev, libbsd-dev, automake, dh-exec
Standards-Version: 4.5.0
Vcs-Git: https://salsa.debian.org/kitterman/opendmarc.git
Vcs-Browser: https://salsa.debian.org/kitterman/opendmarc
Homepage: http://www.trusteddomain.org/opendmarc

Package: opendmarc
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}, adduser, lsb-base, publicsuffix,
 dbconfig-mysql | dbconfig-no-thanks
Recommends: libopendbx1, libopendbx1-mysql, perl, libswitch-perl, libdbi-perl,
 libdbd-mysql-perl, libhttp-message-perl, libjson-perl, ${perl:Depends}
Suggests: python, python-mysqldb, libxml-simple-perl, libmime-tools-perl
Description: Milter implementation of DMARC
 Domain-based Message Authentication, Reporting and Conformance (DMARC),
 builds on the successes of technologies such as DomainKeys Identified Mail
 (DKIM) and the Sender Policy Framework (SPF) to create an infrastructure that
 enforces policy on domain names that are visible to end users, and creates a
 feedback framework for identifying and tracking fraudulent use of domain
 names in email.  It uses OpenDBX as an interface layer to various SQL back
 ends.
 .
 It provides the following new capabilities:
 .
 A binding between the domain name seen in the From: field of a message and
 one or both of the domain names verified by DKIM and SPF;
 .
 The capability to request that receivers enforce strict message
 authentication policy published by the author; and Comprehensive reporting,
 both forensic and aggregate, regarding suspect messages.
 .
 This package contains the OpenDMARC mail filter (Milter) for plugging into
 Milter-aware MTAs. It implements support for both message reject and DMARC
 failure reporting.
 .
 It also provides various example scripts which can be used for enhancing
 opendmarc.

Package: libopendmarc2
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Library for DMARC validation and reporting
 Domain-based Message Authentication, Reporting and Conformance (DMARC),
 builds on the successes of technologies such as DomainKeys Identified Mail
 (DKIM) and the Sender Policy Framework (SPF) to create an infrastructure that
 enforces policy on domain names that are visible to end users, and creates a
 feedback framework for identifying and tracking fraudulent use of domain
 names in email.
 .
 This package provides library for implementing mail validation and reporting
 for the experimental DMARC standard.

Package: libopendmarc-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libopendmarc2 (= ${binary:Version}), ${misc:Depends}
Description: Headers and development libraries for the OpenDMARC library
 Domain-based Message Authentication, Reporting and Conformance (DMARC),
 builds on the successes of technologies such as DomainKeys Identified Mail
 (DKIM) and the Sender Policy Framework (SPF) to create an infrastructure that
 enforces policy on domain names that are visible to end users, and creates a
 feedback framework for identifying and tracking fraudulent use of domain
 names in email.
 .
 This package provides the required header files and development libraries for
 developing against the OpenDMARC library.
