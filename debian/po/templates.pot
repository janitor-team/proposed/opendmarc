# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the opendmarc package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: opendmarc\n"
"Report-Msgid-Bugs-To: opendmarc@packages.debian.org\n"
"POT-Creation-Date: 2017-10-20 14:06-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../opendmarc.templates:1001
msgid "Configure database for OpenDMARC with dbconfig-common?"
msgstr ""

#. Type: boolean
#. Description
#: ../opendmarc.templates:1001
msgid ""
"To generate aggregate feedback reports a database is needed. This can be "
"optionally handled with dbconfig-common."
msgstr ""

#. Type: boolean
#. Description
#: ../opendmarc.templates:1001
msgid ""
"If you are an advanced database administrator and know that you want to "
"perform this configuration manually, or if your database has already been "
"installed and configured, you should refuse this option. Details on what "
"needs to be done are provided in /usr/share/doc/opendmarc/README.Debian."
msgstr ""

#. Type: boolean
#. Description
#: ../opendmarc.templates:1001
msgid "Otherwise, you should probably choose this option."
msgstr ""
