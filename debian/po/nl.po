# Dutch translation of opendmarc debconf templates.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the opendmarc package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: opendmarc\n"
"Report-Msgid-Bugs-To: opendmarc@packages.debian.org\n"
"POT-Creation-Date: 2017-10-20 14:06-0700\n"
"PO-Revision-Date: 2018-12-27 20:30+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 2.91.7\n"

#. Type: boolean
#. Description
#: ../opendmarc.templates:1001
msgid "Configure database for OpenDMARC with dbconfig-common?"
msgstr "Een databank voor OpenDMARC configureren met dbconfig-common?"

#. Type: boolean
#. Description
#: ../opendmarc.templates:1001
msgid ""
"To generate aggregate feedback reports a database is needed. This can be "
"optionally handled with dbconfig-common."
msgstr ""
"Voor het genereren van samengestelde feedbackrapporten is een databank "
"nodig. Dit kan desgewenst afgehandeld worden met dbconfig-common."

#. Type: boolean
#. Description
#: ../opendmarc.templates:1001
msgid ""
"If you are an advanced database administrator and know that you want to "
"perform this configuration manually, or if your database has already been "
"installed and configured, you should refuse this option. Details on what "
"needs to be done are provided in /usr/share/doc/opendmarc/README.Debian."
msgstr ""
"Indien u een ervaren databankbeheerder bent en ervan overtuigd bent dat u "
"deze configuratie handmatig wilt uitvoeren, of indien uw databank reeds "
"geïnstalleerd en geconfigureerd werd, moet u niet kiezen voor deze "
"mogelijkheid. Meer informatie over wat er moet gebeuren, wordt gegeven in /"
"usr/share/doc/opendmarc/README.Debian."

#. Type: boolean
#. Description
#: ../opendmarc.templates:1001
msgid "Otherwise, you should probably choose this option."
msgstr "In het andere geval moet u wellicht wel voor deze optie kiezen."
